extends Node

var ping_count = 1;
var connecting_count = 0;

var client = null;
var wrapped_client = null;

const MASTER_IP = "127.0.0.1";
const MASTER_PORT = 1337;

const INIT_HEARTBEAT = "init_register_server";
const HEARTBEAT_PING = "keepalive_server";

func _ready():
	client = StreamPeerTCP.new();
	connect_to_server(MASTER_IP, MASTER_PORT);

func _on_Timer_timeout():
	if client.is_connected_to_host():
		print ("Sending heartbeat to master server...");
		
		wrapped_client.put_var(HEARTBEAT_PING);
		ping_count += 1;
	else:
		print ("Attempt to send heartbeat to master server failed, we are not connected.");

func _process(delta):
	if client.is_connected_to_host() && wrapped_client.get_available_packet_count() > 0:
		print("Received: ", wrapped_client.get_var());
	
	if client.get_status() == 1:
		connecting_count += delta;
	
	if connecting_count > 1:
		print("Stuck connecting, try to restart the server");
		client.disconnect_from_host();
		set_process(false);

func connect_to_server(host, port):
	if !client.is_connected_to_host():
		print("Connected to MASTER_SERVER, sending registration request");
		# Connect to server
		client.connect_to_host(host, port);

		# Wrap the StreamPeerTCP in a PacketPeerStream
		wrapped_client = PacketPeerStream.new()
		wrapped_client.set_stream_peer(client)
		wrapped_client.put_var(INIT_HEARTBEAT);
		set_process(true) # start listening for packets
	else:
		print("Already connected...");