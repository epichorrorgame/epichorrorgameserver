extends KinematicBody

puppet var camera_angle = 0;
var mouse_sensitivity = 0.3;

puppet var puppet_velocity = Vector3();
puppet var puppet_direction = Vector3();
puppet var puppet_position = Transform();

# Fly stuff
const FLY_SPEED = 40;
const FLY_ACCEL = 4;

# Walk stuff
var gravity = -9.8 * 3;
const MAX_SPEED = 8;
const MAX_RUNNING_SPEED = 20;
const ACCEL = 1.5;
const DEACCEL = 8;

#jumping
var jump_height = 10;
var AIR_SPEED = 15;

func _ready():
	var player_id = get_network_master();
	
	puppet_position = global_transform;

func _physics_process(delta):
	global_transform = puppet_position;
	move_and_slide(puppet_velocity, Vector3(0, 1, 0), 0.10);
	
	puppet_position = global_transform;