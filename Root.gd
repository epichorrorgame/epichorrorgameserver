extends Node

const PORT        = 1338
const MAX_PLAYERS = 8

var map_info = {};
var players = {};

func get_rand_range(from, to):
	return range(from, to)[randi() % range(from, to).size()];

func _ready():
	var server = NetworkedMultiplayerENet.new();
	server.create_server(PORT, MAX_PLAYERS);

	pick_random_map();

	get_tree().set_network_peer(server);

	get_tree().connect("network_peer_connected", self, "_player_connected");
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected");

func _player_connected(id):
	print('Client ' + str(id) + ' connected to Server');

func _player_disconnected(id):
	print('Client ', str(id), ' disconnected from the server.');
	
	rpc("player_left", id);

remotesync func player_left(client_id):
	print("Player ", players[client_id].display_name, " left the server.");
	
	players.erase(client_id);
	# Delete player node
	get_tree().get_root().get_node('/root/Root/World/Players/' + str(client_id)).queue_free();

func pick_random_map():
	print("Setting up world...");

	# Pick a random map
	var maps = $'/root/MapsList'.maps;
	map_info = maps[get_rand_range(0, maps.size())];

	print("Map ", map_info, " chosen.");

remote func _done_loading_game(client_id):
	# Check if player has already been set to ready. U can think of this as a security check so we dont spawn multiple instances of the same player
	if (!players[client_id].done_loading):
		# Spawn player node
		var player_node = load("res://Player.tscn").instance();
		player_node.set_name(str(client_id));
		player_node.set_network_master(client_id);
		$'/root/Root/World/Players'.add_child(player_node);
		
		for p in players:
			rpc_id(p, "done_loading_game", client_id);

remote func _register_player(r_packet):
	print(r_packet);
	var id = r_packet.client_id;
	var player = r_packet.player;
	
	var spawn_point = map_info.spawn_points[get_rand_range(0, map_info.spawn_points.size())];
	
	players[id] = {
		client_id = id,
		display_name = player.display_name,
		position = spawn_point,
		done_loading = false
	};

	var s_packet = {
		player = players[id],
		map = map_info,
		players = players
	};

	# Send game state info to the new player
	rpc_id(id, "_joined_server", s_packet);
	
	for p in players:
		if( p != get_tree().get_network_unique_id()):
			rpc_id(p, "register_player", s_packet.player); # s_packet.player is the only thing the already existing players need.